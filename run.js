var http = require('http')
var cmd = require('node-cmd')
var createHandler = require('node-gitlab-webhook')
const PROJECT_PATH = ''
var handler = createHandler([ // 多个仓库
  { path: '/web1', secret: 'haha' },
  { path: '/web2', secret: 'haha' },
])

var handler = createHandler({ path: '/web', secret: 'haha' }) // 单个仓库

http.createServer(function (req, res) {
  handler(req, res, function (err) {
    res.statusCode = 404
    res.end('no such location')
  })
}).listen(8081)

handler.on('error', function (err) {
  console.error('Error:', err.message)
})

handler.on('push', function (event) {
  let date = new Date();
  let ymd = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`
  let time = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
  console.log(
    '[%s %s] Received a push event for %s to %s',
    ymd,
    time,
    event.payload.repository.name,
    event.payload.ref
  )
  switch (event.path) {
    case '/web1':
        cmd.get(`git_pull.cmd ${PROJECT_PATH}`,(err,data,stderr)=>{
          console.log(data);
        })
      break
  }
})
